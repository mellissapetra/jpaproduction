<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package jpaproductions
 */
 global $theme_options;
?>
	<footer class="site-footer" role="contentinfo">
		<div class="footer_top">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-4">
                          <div class="footer_menus">
                                <div class="footer-menu">
                                <?php
                                    wp_nav_menu( array(
                                        'theme_location' => 'footer',
                                        'depth' => 2,
                                        'menu_class' => 'list-unstyled',

                                    ) );
                                ?>
                        <div class="footer-social">
                            <div class="facebook-tb">
                                    <?php if($theme_options['facebook-link'] != ''){?>

                            <a class="icon_set" href="<?php echo $theme_options['facebook-link']?>"><img src="<?php echo get_template_directory_uri()?>/assets/images/facebook.png"><p>Facebook</p></a>
                                <?php }?>
                             </div>
                            <div class="facebook-tb">
                                    <?php if($theme_options['twitter-link'] != ''){?>
                                    <a class="icon_set" href="<?php echo $theme_options['twitter-link']?>"> <img src="<?php echo get_template_directory_uri()?>/assets/images/instagram.png"><p>Twitter</p></a>
                                    <?php }?>
                            </div>
                            <div class="facebook-tb">
                                <?php if($theme_options['instagram-link'] != ''){?>
                                <a class="icon_set" href="<?php echo $theme_options['instagram-link']?>"><img src="<?php echo get_template_directory_uri()?>/assets/images/twitter.png"><p>Instagram </p></a>
                                <?php }?>
                            </div>
                        </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-5">

                    </div>
                    <div class="col-md-3">

                        <div class="footer-form">
                            <?php echo do_shortcode('[contact-form-7 id="5" title="Subscribe Form"]'); ?>
                        </div>
                    </div>
                </div>
                <div class="footer_bottom ">

            	<div class="row">
                    <div class="col-md-12">
                        <?php if($theme_options['copy-right-text'] != ''){?>
                        <div class="copy-right">
                        	<p><?php echo $theme_options['copy-right-text'];?></p>
                        </div>
                        <?php }?>
                    </div>
                </div>

        </div>
            </div>
        </div>

    </footer>
<?php wp_footer(); ?>

</body>
</html>