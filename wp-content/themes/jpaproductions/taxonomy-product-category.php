<?php
/**
 * The template for displaying archive pages.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Wp New_setup
 */

get_header(); ?>

		<div class="cat-banner">
					<?php
					$term = get_term_by( 'slug', 
							get_query_var( 'term' ), 
							get_query_var( 'taxonomy' ) ); 
							$category_image = get_field('category_image', $term);
							$category_text = get_field('category_text', $term);
					?>

		<?php if($category_image) { ?>
		<div class="">			
			<img src="<?php echo $category_image; ?>" class="" alt="" loading="lazy">			
		</div>
		<?php } else { ?>
		
		<div class="">			
			<img src="<?php echo get_template_directory_uri(); ?>/assets/images/no-image.jpg" alt="No Image Found" />
		</div>
		<?php } ?>

		</div>
	

		<div class="cat-banner-name">
			<?php echo $term->name; ?>
		</div>
	

	<div class="category-sec">
		 <div class="cat-custom-field">
		  <?php echo $category_text; ?>
		 </div>
		
		 <div class="cat-description">
		 	<?php the_archive_description(); ?>
		 </div>
	</div>

	<div class="cat-post">
		<div class="row">
		<?php 
		while ( have_posts() ) : the_post(); ?>
			<div class="col-md-6">
			<div class="cat-img">
				<?php
					if(has_post_thumbnail()){
						the_post_thumbnail("cat_thumb");
					}else{
						echo '<img src="'.get_template_directory_uri().'/assets/images/no-image.jpg" alt="No Image Found">';
					}
				?>
			</div>
			<div class="cat-title">
				<?php the_title(); ?>
			</div>
		</div>
	 	<?php endwhile; ?>
	 </div>
	</div>

<?php get_footer(); ?> 