<?php
/**
 * The template for displaying all pages.
 * @package Wp New_setup
 */

get_header(); ?>

	<?php
	while ( have_posts() ) : the_post();

		get_template_part( 'template-parts/content', 'page' );

	endwhile; ?>

<?php get_footer();