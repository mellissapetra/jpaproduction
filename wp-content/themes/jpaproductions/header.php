<?php
/**
 * The header for our theme.
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package jpaproductions
 */
global $theme_options;
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="viewport" content="initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<?php if ( is_singular() && pings_open( get_queried_object() ) ) : ?>
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<?php endif; ?>
	<?php wp_head(); ?>
    <script>
    var jpaproductions_ajax_url = "<?php echo admin_url('admin-ajax.php'); ?>";
    </script>
</head>
<body <?php body_class(); ?>>
<?php
    $loop_count = 0;
    if (has_post_thumbnail()) {
        $loop_count =1;
    }
?>
<header id="header" <?php echo $loop_count == 1 ? 'class="top_header"' : 'class="top_header no_banner"'; ?>>


    <?php  ?>
<div class="navbar navbar-inverse">
    <div class="container-fluid">

        <div id="main_navbar" class="header-menu">
            <nav class="navbar navbar-expand-lg navbar-dark">
                <?php if($theme_options['header-logo'] != ''){?>
                    <a class="navbar-brand" href="<?php echo site_url()?>" title="<?php echo bloginfo('name')?>">
                        <img src="<?php echo $theme_options['header-logo']?>" alt="<?php echo bloginfo('name')?>"/>
                    </a>
                    <?php }?>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="collapsibleNavbar">

                    <?php
                    wp_nav_menu( array(
                        'theme_location' => 'primary',
                        'depth' => 2,
                        'container' => false,
                        'menu_class' => 'navbar-nav ml-auto',
                    ) );

                	?>
                </div>
            </nav>
    	</div>
    </div>
   </div>
</header>
