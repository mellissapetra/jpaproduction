<?php /* Template Name: Film Page Template*/
	global $theme_options;
	get_header();
 ?>

<div class="main-container">
	<div class="film-banner">
		<div class="film-banner-img">			
		<?php  	
				echo '<img src="'.get_template_directory_uri().'/assets/images/no-image.jpg" alt="No Image Found">';			
		?>
											
		</div>
		<div class="film-text">
			<h1><?php the_title(); ?></h1>
		</div>
	</div>


	<div class="container">
		<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

			<div class="detail-wrap">


				<div class="entry-content">
					<?php
						while ( have_posts() ) : the_post();
							the_content();
						endwhile;
					?>
				</div><!-- .entry-content -->
			</div>


		</article><!-- #post-## -->
	</div>
</div>
<?php get_footer(); ?>