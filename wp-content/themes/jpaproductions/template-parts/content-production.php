<?php
/**
 * Template part for displaying posts.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Wp New_setup
 */
get_header();
?>
<div class="container-fluid banner">
	<div class="single-banner">
	<?php if ( has_post_thumbnail() ) {
			the_post_thumbnail();
		} else { ?>
	<img src="<?php echo get_template_directory_uri(); ?>/assets/images/no-image.jpg" alt="No Image Found" />
	<?php } ?>
	</div>
	<div class="single-title">
		<?php the_title (); ?>
	</div>
</div>




<div class="container">
	<div class="entry-content">
		<?php

			if( is_single() ){

				the_content( sprintf(
					/* translators: %s: Name of current post. */
					wp_kses( __( 'Continue reading %s <span class="meta-nav">&rarr;</span>', 'jpaproductions' ), array( 'span' => array( 'class' => array() ) ) ),
					the_title( '<span class="screen-reader-text">"', '"</span>', false )
				) );
			} else{

				the_excerpt();
			}

		?>
	</div><!-- .entry-content -->
		</div>


<?php get_footer(); ?>
