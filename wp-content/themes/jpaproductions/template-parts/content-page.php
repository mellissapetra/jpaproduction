<?php
/**
 * Template part for displaying page content in page.php.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Wp New_setup
 */

?>
<div class="main-container">
	<?php 
	    $loop_count = 0;
	    if (has_post_thumbnail()) {
	        $loop_count =1;
	    }
	?>
	<div <?php echo $loop_count == 1 ? 'class="page_banner"' : 'class="page_banner no_banner-image"'; ?>>
		<div class="banner-top">
			<?php the_post_thumbnail(); ?>
		</div>
		<div class="banner_text">
			<div class="container">
				<div class="bg_black">
					<label> 
						<?php 
							$sub_title = get_field('sub_title');	
							if (isset($sub_title) && $sub_title != '') {
							 	echo get_field('sub_title'); 
							}
							else{
							?>
							<?php
							}
						?>					
					</label>
					
					<?php 
							$delivery_text = get_field('delivery_text');
						?>
					<?php if ( is_front_page() && isset($delivery_text) && $delivery_text != '') { ?>
						<div class="delivery_text"><?php echo get_field('delivery_text'); ?></div>
					<?php } ?>
				</div>
			</div>
		</div>
	</div>

	<div class="container">
		<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
			
			<div class="detail-wrap">
				

				<div class="entry-content">
					<?php
						the_content();

						wp_link_pages( array(
							'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'jpaproductions' ),
							'after'  => '</div>',
						) );
					?>
				</div><!-- .entry-content -->
			</div>

			
		</article><!-- #post-## -->
	</div>
</div>