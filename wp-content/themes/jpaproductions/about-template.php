<?php /* Template Name: About Page Template*/
	global $theme_options;
	get_header();
 ?>

<div class="main-container">
	<?php
	    $loop_count = 0;
	    if (has_post_thumbnail()) {
	        $loop_count =1;
	    }
	?>
	<div <?php echo $loop_count == 1 ? 'class="page_banner"' : 'class="page_banner no_banner-image"'; ?>>
		<div class="banner-top">
			<?php the_post_thumbnail(); ?>

		<div class="banner_text">
		<div class="container">
			<div class="bg_text">
					<?php echo get_field('sub_title'); ?>
				</div>
				<div class="bg_black">
					<h1 class="page_title">
						<?php the_title(); ?>
					</h1>
				</div>

			</div>
		</div>
		</div>

	</div>


	<div class="container">
		<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

			<div class="detail-wrap">


				<div class="entry-content">
					<?php
						while ( have_posts() ) : the_post();
							the_content();
						endwhile;
					?>
				</div><!-- .entry-content -->
			</div>


		</article><!-- #post-## -->
	</div>
</div>
<?php get_footer(); ?>