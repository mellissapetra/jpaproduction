<?php /* Template Name: CMS Template*/
	global $theme_options;
	get_header(); 
 ?>

<div class="main-container">
	<?php 
	    $loop_count = 0;
	    if (has_post_thumbnail()) {
	        $loop_count =1;
	    }
	?>
	<div <?php echo $loop_count == 1 ? 'class="page_banner"' : 'class="page_banner no_banner-image"'; ?>>
		<div class="banner-top">
			<?php the_post_thumbnail(); ?>
		</div>
		<div class="banner_text">
			<div class="container">
				<div class="bg_black">
					<h1 class="page_title">				
						<?php 
							$main_title = get_field('main_title');
							// var_dump($main_title); exit();
						?>
						<?php 
							/*if(!empty($main_title)){}*/
							if (isset($main_title) && $main_title != '') {
								//print_r($main_title); die;
							?>
								<span>
									<?php echo get_field('main_title'); ?>
								</span>
							<?php	
							}
							else{
							?>
								<span><?php the_title(); ?></span>						
							<?php
							}
						?>
					</h1>
					<label> 
						<?php 
							$sub_title = get_field('sub_title');	
							if (isset($sub_title) && $sub_title != '') {
							 	echo get_field('sub_title'); 
							}
							else{
							?>
							<span><?php the_title(); ?></span>
							<?php
							}
						?>					
					</label>
				</div>
			</div>
		</div>
	</div>
	

	<div class="container">
		<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
			
			<div class="detail-wrap">
				

				<div class="entry-content">
					<?php 
						while ( have_posts() ) : the_post();
							the_content(); 
						endwhile;
					?>
				</div><!-- .entry-content -->
			</div>

			
		</article><!-- #post-## -->
	</div>
</div>
<?php get_footer(); ?>