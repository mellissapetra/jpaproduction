<?php
/*
Element Description: VC SimDeal Box
*/
 
class vcSimDealBox extends WPBakeryShortCode {
     
    function __construct() {
        add_action( 'init', array( $this, 'vc_simdealbox_mapping' ) );
        add_shortcode( 'vc_simdealbox', array( $this, 'vc_simdealbox_html' ) );
    }
     
    public function vc_simdealbox_mapping() {
         
          // Stop all if VC is not enabled
        if ( !defined( 'WPB_VC_VERSION' ) ) {
                return;
        }
             
        // Map the block with vc_map()
        vc_map( 
      
            array(
                'name' => __('Sim Deal', 'jpaproductions'),
                'base' => 'vc_simdealbox',
                'description' => __('Sim Deal VC box', 'jpaproductions'), 
                'category' => __('Wp New_setup Elements', 'jpaproductions'),   
                // 'icon' => get_template_directory_uri().'/assets/images/deal-box.png',
                'params' => array(   
                          
                    array(
                        'type' => 'attach_image',
                        'holder' => 'img',
                        'class' => 'data-class',
                        'heading' => __( 'Sim Logo', 'jpaproductions' ),
                        'param_name' => 'sim_logo',
                        'admin_label' => false,
                        'weight' => 0,
                    ), 

                    array(
                        'type' => 'textfield',
                        'heading' => __( 'Logo Title', 'jpaproductions' ),
                        'param_name' => 'logo_title',
                        'admin_label' => false,
                        'weight' => 0,
                    ),

                    array(
                        'type' => 'textfield',
                        'class' => 'minutes-class',
                        'heading' => __( 'Minutes', 'jpaproductions' ),
                        'param_name' => 'minutes',
                        'admin_label' => false,
                        'weight' => 0,
                    ),
    				
    				array(
                        'type' => 'textfield',
                        'class' => 'texts-class',
                        'heading' => __( 'Texts', 'jpaproductions' ),
                        'param_name' => 'texts',
                        'admin_label' => false,
                        'weight' => 0,
                    ),

                    array(
                        'type' => 'textfield',
                        'heading' => __( 'Data', 'jpaproductions' ),
                        'param_name' => 'data',
                        'admin_label' => true,
                        'weight' => 0,
                    ),

                    array(
                        'type' => 'textfield',
                        'heading' => __( 'Contract', 'jpaproductions' ),
                        'param_name' => 'contract',
                        'admin_label' => false,
                        'weight' => 0,
                    ),  
                      
    				array(
                        'type' => 'textfield',
                        'class' => 'price-class',
                        'heading' => __( 'Price', 'jpaproductions' ),
                        'param_name' => 'price',
                        'admin_label' => false,
                        'weight' => 0,
                    ), 
    				
    				array(
                        'type' => 'textfield',
                        'class' => 'price-text-class',
                        'heading' => __( 'Price Text', 'jpaproductions' ),
                        'param_name' => 'price_text',
                        'admin_label' => false,
                        'weight' => 0,
                    ),
    				
    				array(
                        'type' => 'textfield',
                        'class' => 'link-class',
                        'heading' => __( 'Link', 'jpaproductions' ),
                        'param_name' => 'link',
                        'admin_label' => false,
                        'weight' => 0,
                    ),
    				
                    array(
                        'type' => 'textfield',
                        'class' => 'price-text-class',
                        'heading' => __( 'Order from', 'jpaproductions' ),
                        'param_name' => 'order_from',
                        'admin_label' => false,
                        'weight' => 0,
                    ),

    				array(
    					"type" => "colorpicker",
    					"class" => "color-class",
    					"heading" => __( "Background Color", "jpaproductions" ),
    					"param_name" => "color",
    					"value" => '#fdcb0a',
    					"description" => __( "Choose text Background color", "jpaproductions" )
    				 ),

                    array(
                        'type' => 'attach_image',
                        // 'holder' => 'img',
                        'class' => 'data-class',
                        'heading' => __( 'Background image', 'jpaproductions' ),
                        'param_name' => 'background_image',
                        'admin_label' => false,
                        "description" => __( "Choose Price Box Background Image", "jpaproductions" ),
                        'weight' => 0,
                    ), 
    				 
    			)
            )
        );                   
    }

    public function vc_simdealbox_html( $atts ) {
         
           // Params extraction
    extract(
       $a =  shortcode_atts(
            array(
                'sim_logo'   => 'sim_logo',
                'logo_title' => '',
                'data'   => '',
                'minutes' => '',
				'texts' => '',
				'price' => '',
				'price_text' => '',
				'link' => '',
				'color'=>'',
				'contract'=>'',
                'order_from'=>'',
                'background_image'=>''
            ),

            $atts
        )
    );

    $img = wp_get_attachment_image_src($a["sim_logo"], "large");
    $imgSrc = $img[0];
    $img_bg = wp_get_attachment_image_src($a["background_image"], "large");
    $imgSrc = $img_bg[0];
    $logo_div = '';
    if($img){
        $logo_div='<div class="row sim_logo_box">
            <div class="col-md-4">
                <img class="img-fluid" src="' . $img[0] .'" alt="image"/>
                <span>'. $logo_title .'</span>
            </div>
        </div>';
    }

    $minutes_div = '';
    if($minutes){
        $minutes_div ='<strong>'.$minutes.'</strong>
                            <span>'. __( 'Minutes', 'jpaproductions' ).'</span>';
    }

    $texts_div = '';
    if($texts){
        $texts_div = '<strong>'.$texts.'</strong> <span>'. __( 'texts', 'jpaproductions' ).'</span>';
    }

    $data_div='';
    if ($data) {
        $data_div='<strong class="text-uppercase">'.$data.'</strong> <span>'. __( 'Data', 'jpaproductions' ).'</span>';
    }

    $contract_div='';
    if ($contract) {
        $contract_div='<strong>'.$contract.'</strong> <span>'. __( 'Contract', 'jpaproductions' ).'</span>';
    }

    $price_div='';
    if ($price) {
        $price_div='<strong>'.$price.'</strong>';
    }

    $price_text_div='';
    if ($price_text) {
        $price_text_div='<span>'.$price_text.'</span>';
    }

    $link_div='';
    if ($link) {
        $link_div='<a href="'.$link.'" class="see_more_btn" style="color:'.$color.'">'. __( 'See deal', 'jpaproductions' ).' <i style="background-color:'.$color.';border-color:'.$color.'"></i> </a>';
    }

    $order_from_div='';
    if ($order_from) {
        $order_from_div='<div class="text-center">'. __( 'Order from', 'jpaproductions' ).' <b> '.$order_from.' </b></div>';
    }

    // Fill $html var with data
    $html = '
    <div class="vc-simdealbox-wrap">
        '.$logo_div.'
        <div class="row sim_detail_box equal_container">
            <div class="col-md-4">
                <div class="row">
                    <div class="col-md-6 col-sm-6 equal_height"> '. $minutes_div .' </div>
                    <div class="col-md-6 col-sm-6 equal_height"> ' . $texts_div . ' </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="row">
                    <div class="col-md-6 col-sm-6 equal_height"> ' . $data_div . ' </div>
                    <div class="col-md-6 col-sm-6 equal_height"> ' . $contract_div . '</div>
                </div>
            </div>
             <div class="col-md-4" style="background-color:'.$color.'; border-color:'.$color.'">
             <div class="price_box equal_height" style="background: url('. $img_bg[0] .');">
               <div class="row">
                    <div class="col-md-5 col-sm-6">
                        '.$price_div.'
                        '.$price_text_div.'
                    </div>
                    <div class="col-md-7 col-sm-6">
                        ' . $link_div . '
                        '. $order_from_div .'
                    </div>
                </div>
            </div>
            </div>
        </div>
    </div>';      
     
    return $html;
    } 


} 

new vcSimDealBox();  