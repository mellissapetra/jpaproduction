<?php 
global $option_items;

//Header Fields Starts	
$option_items[]= array(
					'type'=>'section',
					'name'=>'header',
					'title'=>'Header',
					'desc'=>'You can manage header settings from here',
				 );

$option_items[]= array(
					'type'=>'image',
					'name'=>'header-logo',
					'title'=>'Logo',
					'desc'=>'You can manage header logo from here',
				 );	

//Header Fields Ends

	
//Footer Fields Start				 
$option_items[]= array(
					'type'=>'section',
					'name'=>'footer',
					'title'=>'Footer',
					'desc'=>'You can manage footer settings from here',
				 );

$option_items[]= array(
					'type'=>'text',
					'name'=>'facebook-link',
					'title'=>'Facebook Link',
					'desc'=>'You can manage footer facebook link from here',
				 );		
$option_items[]= array(
					'type'=>'text',
					'name'=>'twitter-link',
					'title'=>'Twitter Link',
					'desc'=>'You can manage footer twitter link from here',
				 );		
$option_items[]= array(
					'type'=>'text',
					'name'=>'instagram-link',
					'title'=>'Instagram Link',
					'desc'=>'You can manage footer instagram link from here',
				 );		

$option_items[]= array(
					'type'=>'text',
					'name'=>'copy-right-text',
					'title'=>'Copyright',
					'desc'=>'You can manage footer Copyright text from here',
				 );		
//Footer Fields Ends			 				 			 
?>