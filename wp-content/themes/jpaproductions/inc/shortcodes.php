<?php
/*
Shortcode : [sliders] sliders Page
*/
	add_shortcode('sliders', 'fn_sliders');
	function fn_sliders($attr){
	ob_start();
	?>
			<?php
				$temp = $wp_query; $wp_query= null;
				 $args = array(
					'posts_per_page'   => 4,
					'post_type'     => 'slider',
					'post_status' => 'publish',
				);
				$wp_query = new WP_Query(); $wp_query->query($args);
				if (isset($attr['posts_per_page']) && $attr['posts_per_page'] != '') {
					$args['posts_per_page'] = 6;
				}

				if($wp_query->have_posts()):?>
					<div class="slider-main">
						<?php while ($wp_query->have_posts()) : $wp_query->the_post(); ?>

								<div class="slider_box">
									<div class="slider-img">
										<?php
											if(has_post_thumbnail()){
												the_post_thumbnail("post_thumb");
											}else{
												echo '<img src="'.get_template_directory_uri().'/assets/images/no-image.jpg" alt="No Image Found">';
											}
										?>
									</div>
									<a href="<?php echo get_field('slide_link'); ?>" class="slider-link">
									<div class="slider-title">
										<h4><?php the_title(); ?></h4>
									</div>
									</a>
								</div>

						<?php endwhile;?>
					</div>
				<?php endif; ?>

			<?php wp_reset_postdata(); ?>

	<?php
	wp_reset_postdata();
	$sliders = ob_get_contents();
	ob_end_clean();
	return $sliders;
	}

/*
Shortcode : [homefeatureproduct] homefeatureproduct Page
*/
	add_shortcode('homefeatureproduct', 'fn_homefeatureproduct');
	function fn_homefeatureproduct($attr){
	ob_start();
	?>
			<?php
				$temp = $wp_query; $wp_query= null;
				  $args = array(
					'posts_per_page'   => -1,
					'orderby' => 'date',
					'order'   => 'DESC',
					'post_type'   => 'production',
					'post_status' => 'publish',
					'meta_query' => array(
						array(
							'key' => 'product_checkbox',
							'value' => true,
							'compare' => '='
						)
					),
				);
				$wp_query = new WP_Query(); $wp_query->query($args);
				if($wp_query->have_posts()):?>
					<div class="row">
						<?php while ($wp_query->have_posts()) : $wp_query->the_post(); ?>
								<div class="col-md-12">
									<div class="product-all">
										<div class="product-all-img">
											<?php
												if(has_post_thumbnail()){
													the_post_thumbnail("feature_thumb");
												}else{
													echo '<img src="'.get_template_directory_uri().'/assets/images/no-image.jpg" alt="No Image Found">';
												}
											?>
										</div>
										<div class="product-all-title">
											<h3><?php the_title(); ?></h3>
										</div>
										<div class="product-all-text">
											<?php the_excerpt(); ?>
										</div>
									</div>
								</div>

						<?php endwhile;?>
					</div>

				<?php endif; ?>
			<?php wp_reset_postdata(); ?>

	<?php
	wp_reset_postdata();
	$homefeatureproduct = ob_get_contents();
	ob_end_clean();
	return $homefeatureproduct;
	}


/*
Shortcode : [homeproductions] productions Page
*/
	add_shortcode('homeproductions', 'fn_homeproductions');
	function fn_homeproductions($attr){
	ob_start();
	?>
			<?php
				$temp = $wp_query; $wp_query= null;
				 $args = array(
					'posts_per_page'   => -1,
					'post_type'     => 'production',
					'post_status' => 'publish',
					'meta_query' => array(
						'relation' => 'OR',
						 array(
							'key'     => 'product_checkbox',
							'value'   => '0',
							'compare' => '='
								),
						array(
							'key'     => 'product_checkbox',
							'compare' => 'NOT EXISTS'
					)
				)
			);
			$wp_query = new WP_Query(); $wp_query->query($args);
			if (isset($attr['posts_per_page']) && $attr['posts_per_page'] != '') {
					$args['posts_per_page'] = 6;
			}
			if($wp_query->have_posts()):?>
					<div class="row">
						<?php
						$count = 0;
						while ($wp_query->have_posts()) : $wp_query->the_post();
						$count++;
						//echo "<pre>"; print_r($count);
						?>
							<?php if( $count < 7 ){ ?>
								<div class="col-md-4 custom_box">
									<?php }else{ ?>
										<div class="col-md-12">
									<?php } ?>
								<?php $terms = get_the_terms( $post->ID, 'product-category' ); ?>
								<a href="<?php echo home_url()."/product-category/".$terms[0]->slug; ?>">
								<div class="product_box">

										<div class="custom-tag">
											<?php
											//echo "<pre>"; print_r($terms);
											if ( !empty( $terms ) ){
												// get the first term
												$term = array_shift( $terms );
												echo $term->name;
											}

											?>
										</div>
										<div class="product-img">
										<?php
											if(has_post_thumbnail()){
												the_post_thumbnail("post_thumb");
											}else{
												echo '<img src="'.get_template_directory_uri().'/assets/images/no-image.jpg" alt="No Image Found">';
											}
										?>
										</div>
										<div class="custom_title">
											<h3><?php the_title(); ?></h3>
										</div>

									<div class="custom_product">

										<div class="product-tag">
											<?php

											$terms = get_the_terms( $post->ID, 'product-category' );
											// echo "<pre>"; print_r($terms);
											if ( !empty( $terms ) ){
												// get the first term
												$term = array_shift( $terms );
												echo $term->name;
											}

											?>
										</div>

										<div class="product-title">
											<h3><?php the_title(); ?></h3>
										</div>

										<div class="product-text">
											<?php the_excerpt(); ?>
										</div>
									</div>
							</div>
						</a>
					</div>
						<?php endwhile; ?>
					</div>
				<?php endif; ?>
			<?php wp_reset_postdata(); ?>

	<?php
	wp_reset_postdata();
	$homeproductions = ob_get_contents();
	ob_end_clean();
	return $homeproductions;
	}


/*
Shortcode : [aboutfeatureproduct] aboutfeatureproduct Page
*/
	add_shortcode('aboutfeatureproduct', 'fn_aboutfeatureproduct');
	function fn_aboutfeatureproduct($attr){
	ob_start();
	?>
			<?php
				$temp = $wp_query; $wp_query= null;
				  $args = array(
					'posts_per_page'   => -1,
					'orderby' => 'date',
					'order'   => 'DESC',
					'post_type'   => 'production',
					'post_status' => 'publish',
					'meta_query' => array(
						array(
							'key' => 'product_checkbox',
							'value' => true,
							'compare' => '='
						)
					),
				);
				$wp_query = new WP_Query(); $wp_query->query($args);
				if($wp_query->have_posts()):?>
					<div class="row">
						<?php while ($wp_query->have_posts()) : $wp_query->the_post(); ?>
							<div class="about-product">
							   <div class="col-md-6">

									<div class="about-all-img">
											<?php
												if(has_post_thumbnail()){
													the_post_thumbnail("post_thumb");
												}else{
													echo '<img src="'.get_template_directory_uri().'/assets/images/no-image.jpg" alt="No Image Found">';
												}
											?>
									</div>
							   </div>
								<div class="col-md-6">
									<div class="about-all-title">
										<h3><?php the_title(); ?></h3>
									</div>
									<div class="about-all-text">
										<?php the_excerpt(); ?>
									</div>
							   </div>
							</div>

						<?php endwhile;?>
					</div>

				<?php endif; ?>
			<?php wp_reset_postdata(); ?>

	<?php
	wp_reset_postdata();
	$aboutfeatureproduct = ob_get_contents();
	ob_end_clean();
	return $aboutfeatureproduct;
	}


/*
Shortcode : [aboutproductions] aboutproductions Page
*/
	add_shortcode('aboutproductions', 'fn_aboutproductions');
	function fn_aboutproductions($attr){
	ob_start();
	?>
			<?php
				$temp = $wp_query; $wp_query= null;
				 $args = array(
					'posts_per_page'   => -1,
					'post_type'     => 'production',
					'post_status' => 'publish',
					'meta_query' => array(
						'relation' => 'OR',
						 array(
							'key'     => 'product_checkbox',
							'value'   => '0',
							'compare' => '='
								),
						array(
							'key'     => 'product_checkbox',
							'compare' => 'NOT EXISTS'
					)
				)
			);
			$wp_query = new WP_Query(); $wp_query->query($args);
			if (isset($attr['posts_per_page']) && $attr['posts_per_page'] != '') {
					$args['posts_per_page'] = 6;
			}
			if($wp_query->have_posts()):?>
					<div class="row">
						<?php
						while ($wp_query->have_posts()) : $wp_query->the_post(); ?>
								<div class="col-md-6 about-box">
									<div class="row">
										<div class="col-md-6">

									<div class="about-img">
										<?php
											if(has_post_thumbnail()){
												the_post_thumbnail("post_thumb");
											}else{
												echo '<img src="'.get_template_directory_uri().'/assets/images/no-image.jpg" alt="No Image Found">';
											}
										?>
									</div>
										</div>
										<div class="col-md-6">
										<div class="about-title">
										<h3><?php the_title(); ?></h3>
									</div>
									<div class="about-text">
										<?php the_excerpt(); ?>
									</div>
								</div>

										</div>
										</div>

						<?php endwhile; ?>
					</div>
				<?php endif; ?>
			<?php wp_reset_postdata(); ?>

	<?php
	wp_reset_postdata();
	$aboutproductions = ob_get_contents();
	ob_end_clean();
	return $aboutproductions;
	}

/*
Shortcode : [latestnews] latestnews Page
*/
	add_shortcode('latestnews', 'fn_latestnews');
	function fn_latestnews($attr){
	ob_start();
	?>
			<?php
				$temp = $wp_query; $wp_query= null;
				 $args = array(
					'posts_per_page'   => 3,
					'post_type'     => 'news',
					'post_status' => 'publish',
				);
				$wp_query = new WP_Query(); $wp_query->query($args);
				if (isset($attr['posts_per_page']) && $attr['posts_per_page'] != '') {
					$args['posts_per_page'] = 6;
				}

				if($wp_query->have_posts()):?>
					<div class="row">
						<?php while ($wp_query->have_posts()) : $wp_query->the_post(); ?>

								<div class="col-md-6">

											<div class="news-title">
												<h3><?php the_title(); ?></h3>
												<?php the_content(); ?>
											</div>											
								</div>
								<div class="col-md-6">
									<div class="news_box-img">
										<?php
											if(has_post_thumbnail()){
												the_post_thumbnail("post_thumb");
											}else{
												echo '<img src="'.get_template_directory_uri().'/assets/images/no-image.jpg" alt="No Image Found">';
											}
										?>
									</div>
								</div>


						<?php endwhile;?>
					</div>
				<?php endif; ?>

			<?php wp_reset_postdata(); ?>

	<?php
	wp_reset_postdata();
	$latestnews = ob_get_contents();
	ob_end_clean();
	return $latestnews;
	}


 ?>