<?php 

/*
* Add New Post (Slider)
*/
	 add_action( 'init', 'fn_all_slider_init' );
		
	function fn_all_slider_init() {
	 		$labels = array(
	 			'name'               => _x( 'Slider', 'post type general name', 'jpaproductions' ),
	 			'singular_name'      => _x( 'Slider', 'post type singular name', 'jpaproductions' ),
	 			'menu_name'          => _x( 'Slider', 'admin menu', 'jpaproductions' ),
	 			'name_admin_bar'     => _x( 'Slider', 'add new on admin bar', 'jpaproductions' ),
	 			'add_new'            => _x( 'Add New', 'Slider', 'jpaproductions' ),
	 			'add_new_item'       => __( 'Add New Slider', 'jpaproductions' ),
	 			'new_item'           => __( 'New Slider', 'jpaproductions' ),
	 			'edit_item'          => __( 'Edit Slider', 'jpaproductions' ),
	 			'view_item'          => __( 'View Slider', 'jpaproductions' ),
 				'all_items'          => __( 'All Slider', 'jpaproductions' ),
	 			'search_items'       => __( 'Search Slider', 'jpaproductions' ),
	 			'parent_item_colon'  => __( 'Parent Slider:', 'jpaproductions' ),
	 			'not_found'          => __( 'No Slider found.', 'jpaproductions' ),
	 			'not_found_in_trash' => __( 'No Slider found in Trash.', 'jpaproductions' )
	 		);

	 		$args = array(
	 			'labels'             => $labels,
	 	        'description'        => __( 'Description.', 'jpaproductions' ),
				'public'             => true,
	 			'publicly_queryable' => false,
	 			'show_ui'            => true,
	 			'show_in_menu'       => true,								
	 			'query_var'          => true,
	 			'capability_type'    => 'post',
	 			'has_archive'        => false,
	 			'hierarchical'       => true,
	 			'menu_position'      => null,
	 			'menu_icon'           => 'dashicons-slides',
	 			'supports'           => array( 'title', 'author', 'thumbnail', 'comments' )
	 		);

	 		register_post_type( 'slider', $args );


	 	}

/*
* Add New Post (Production)
*/
	 add_action( 'init', 'fn_all_production_init' );
		
	function fn_all_production_init() {
	 		$labels = array(
	 			'name'               => _x( 'Production', 'post type general name', 'jpaproductions' ),
	 			'singular_name'      => _x( 'Production', 'post type singular name', 'jpaproductions' ),
	 			'menu_name'          => _x( 'Production', 'admin menu', 'jpaproductions' ),
	 			'name_admin_bar'     => _x( 'Production', 'add new on admin bar', 'jpaproductions' ),
	 			'add_new'            => _x( 'Add New', 'Production', 'jpaproductions' ),
	 			'add_new_item'       => __( 'Add New Production', 'jpaproductions' ),
	 			'new_item'           => __( 'New Production', 'jpaproductions' ),
	 			'edit_item'          => __( 'Edit Production', 'jpaproductions' ),
	 			'view_item'          => __( 'View Production', 'jpaproductions' ),
 				'all_items'          => __( 'All Production', 'jpaproductions' ),
	 			'search_items'       => __( 'Search Production', 'jpaproductions' ),
	 			'parent_item_colon'  => __( 'Parent Production:', 'jpaproductions' ),
	 			'not_found'          => __( 'No Production found.', 'jpaproductions' ),
	 			'not_found_in_trash' => __( 'No Production found in Trash.', 'jpaproductions' )
	 		);

	 		$args = array(
	 			'labels'             => $labels,
	 	        'description'        => __( 'Description.', 'jpaproductions' ),
				'public'             => true,
	 			'publicly_queryable' => true,
	 			'show_ui'            => true,
	 			'show_in_menu'       => true,								
	 			'query_var'          => true,
	 			'capability_type'    => 'post',
	 			'has_archive'        => false,
	 			'hierarchical'       => true,
	 			'menu_position'      => null,
	 			'menu_icon'           => 'dashicons-products',
	 			'supports'           => array( 'title', 'author', 'editor', 'thumbnail', 'comments', 'excerpt' )
	 		);

	 		register_post_type( 'production', $args );
	 	}
	add_action( 'init', 'product_custom_taxonomy', 0 );
	function product_custom_taxonomy() {
		$labels = array(
		    'name' => _x( 'Categories', 'taxonomy general name' ),
		    'singular_name' => _x( 'Type', 'taxonomy singular name' ),
		    'search_items' =>  __( 'Search Categories' ),
		    'all_items' => __( 'All Categories' ),
		    'parent_item' => __( 'Parent Categories' ),
		    'parent_item_colon' => __( 'Parent Categories:' ),
		    'edit_item' => __( 'Edit Category' ), 
		    'update_item' => __( 'Update Category' ),
		    'add_new_item' => __( 'Add New Category' ),
		    'new_item_name' => __( 'New Category Name' ),
		    'menu_name' => __( 'Categories' ),
		  );    
		  register_taxonomy('product-category',array('production'), array(
		    'hierarchical' => true,
		    'labels' => $labels,
		    'show_ui' => true,
		    'show_admin_column' => true,
		    'query_var' => true,
		    'rewrite' => array( 'slug' => 'product-category' ),
		));
    }

/*
* Add New Post (News)
*/
	 add_action( 'init', 'fn_all_news_init' );
		
	function fn_all_news_init() {
	 		$labels = array(
	 			'name'               => _x( 'News', 'post type general name', 'jpaproductions' ),
	 			'singular_name'      => _x( 'News', 'post type singular name', 'jpaproductions' ),
	 			'menu_name'          => _x( 'News', 'admin menu', 'jpaproductions' ),
	 			'name_admin_bar'     => _x( 'News', 'add new on admin bar', 'jpaproductions' ),
	 			'add_new'            => _x( 'Add New', 'News', 'jpaproductions' ),
	 			'add_new_item'       => __( 'Add New News', 'jpaproductions' ),
	 			'new_item'           => __( 'New News', 'jpaproductions' ),
	 			'edit_item'          => __( 'Edit News', 'jpaproductions' ),
	 			'view_item'          => __( 'View News', 'jpaproductions' ),
 				'all_items'          => __( 'All News', 'jpaproductions' ),
	 			'search_items'       => __( 'Search News', 'jpaproductions' ),
	 			'parent_item_colon'  => __( 'Parent News:', 'jpaproductions' ),
	 			'not_found'          => __( 'No News found.', 'jpaproductions' ),
	 			'not_found_in_trash' => __( 'No News found in Trash.', 'jpaproductions' )
	 		);

	 		$args = array(
	 			'labels'             => $labels,
	 	        'description'        => __( 'Description.', 'jpaproductions' ),
				'public'             => true,
	 			'publicly_queryable' => true,
	 			'show_ui'            => true,
	 			'show_in_menu'       => true,								
	 			'query_var'          => true,
	 			'capability_type'    => 'post',
	 			'has_archive'        => false,
	 			'hierarchical'       => true,
	 			'menu_position'      => null,
	 			'menu_icon'           => 'dashicons-admin-post',
	 			'supports'           => array( 'title', 'author', 'editor', 'thumbnail', 'comments', 'excerpt' )
	 		);

	 		register_post_type( 'news', $args );
	 	}